//
//  main.swift
//  Disassembler
//
//  Created by David Skuza on 11/15/16.
//  Copyright © 2016 David Skuza. All rights reserved.
//

import Foundation

func error(withString str: String) -> Never {
    if let data = str.data(using: String.Encoding.utf8) {
        FileHandle.standardError.write(data)
    }
    exit(EXIT_FAILURE)
}

// Check that at least 1 argument is used
guard CommandLine.arguments.count > 1 else {
    error(withString: "No ROM to disassemble\n)")
}

// Check that there is an export option available
guard CommandLine.arguments.count > 2 else {
    error(withString: "No export arguments supplied\n")
}

// Validate the supplied argument as an existing file
var romPath = NSString(string: CommandLine.arguments[1]).expandingTildeInPath
var romData: Data

// Initialize a ROM and attempt to read its data, catching any errors
let rom = ROM(romPath)
do {
    romData = try rom.read()
} catch let err as ROMError {
    switch err {
    case .loadFailure:
        error(withString: "ROM \(romPath) does not exist")
    case .readFailure(let str):
        error(withString: str)
    }
}

// Given valid ROM data, 
// initialize a lexer, 
// tokenize the data,
// and print lex'd information
var lexer = Lexer(romData)
lexer.tokenize()

// TODO: Check if an export option is supplied before tokenizing
// ./command /path/to/ROM --print
// ./command /path/to/ROM --output
if CommandLine.arguments.count > 2 {
    switch CommandLine.arguments[2] {
    case "-p", "--print":
        print(lexer.output())
    case "-o", "--output":
        guard CommandLine.arguments.count > 3 else {
            error(withString: "No output path supplied\n")
        }

        let outputPath = NSString(string: CommandLine.arguments[3]).expandingTildeInPath
        do {
            try lexer.output().write(toFile: outputPath, atomically: true, encoding: String.Encoding.utf8)
        } catch let err {
            error(withString: err.localizedDescription)
        }
    default:
        error(withString: "Unknown argument \(CommandLine.arguments[2])\n")
    }
}
