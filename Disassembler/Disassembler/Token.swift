//
//  Token.swift
//  Disassembler
//
//  Created by David Skuza on 11/15/16.
//  Copyright © 2016 David Skuza. All rights reserved.
//

import Foundation

// Add a protocol and extensions for pretty-printing UInt(8|16) as hex
private protocol Hexable {
    func toHex() -> String
}

extension UInt16: Hexable {
    func toHex() -> String {
        return String(format: "%2X", self)
    }
}

extension UInt8: Hexable {
    func toHex() -> String {
        return String(format: "%2X", self)
    }
}

// Add basic token types to define reusable "tuples"
// CustomStringConvertible will be one of String, UInt8, or UInt16
public enum TokenType: CustomStringConvertible {
    case Empty(String)                                                                              // An instruction with no required data (e.g "cls")
    case Single(String, CustomStringConvertible)                                                    // An instruction with one piece of data (e.g "jmp 0")
    case Double(String, CustomStringConvertible, CustomStringConvertible)                           // An instruction with two pieces of data (e.g "add V0 V1")
    case Triple(String, CustomStringConvertible, CustomStringConvertible, CustomStringConvertible)  // An instruction with three pieces of data (e.g "sprite V0 V1 0")

    // Pretty-print enums according to their supplied data
    public var description: String {
        switch self {
        case .Empty(let str):
            return str
        case .Single(let str, let d0):
            return "\(str) \(d0)"
        case .Double(let str, let d0, let d1):
            return "\(str) \(d0) \(d1)"
        case .Triple(let str, let d0, let d1, let d2):
            return "\(str) \(d0) \(d1) \(d2)"
        }
    }
}

// Token:   Describes a CHIP-8 instruction
// http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#Dxyn
// http://www.multigesture.net/wp-content/uploads/mirror/goldroad/chip8_instruction_set.shtml
public struct Token {
    // Expose Token information specific to a ROM instruction

    public var opcode: UInt16   // The computed opcode, when supplied with two bytes
    public var address: UInt16  // The address to where the instruction would be in memory
    public var type: TokenType? // The type of token
    public var containsJump: (Bool, UInt16?) {
        guard self.type != nil else {
            return (false, nil)
        }

        switch self.type! {
        case .Single(let str, let d0):
            let jumps = (str == "jmp" || str == "jsr")
            return (jumps, jumps == true ? d0 as? UInt16 : nil)
        case .Double(let str, _, let d1):
            let jumps = (str == "jmi")
            return (jumps, jumps == true ? d1 as? UInt16 : nil)
        default:
            return (false, nil)
        }
    }

    // Initialize a Token with data to generate a valid opcode
    // byte1:   The hi byte of an opcode
    // byte2:   The lo byte of an opcode
    // address: The address to where the instruction would be in memory
    init(_ byte1: UInt8, _ byte2: UInt8, _ address: UInt16) {
        self.opcode = (UInt16(byte1) << 8) | UInt16(byte2)
        self.address = address
        self.type = self.type(fromOpcode: self.opcode)
    }

    // Generate the type of Token based on opcode
    private func type(fromOpcode: UInt16) -> TokenType? {
        var type: TokenType? = nil
        switch self.hi() & 0xF0 {
        case 0x00:
            if self.lo() == 0xE0 {                                                          // 0x00E0
                type = TokenType.Empty("cls")
            } else if self.lo() == 0xEE {                                                   // 0x00EE
                type = TokenType.Empty("rts")
            } else {                                                                        // 0x0nnn
                type = TokenType.Single("sys", self.nnn())
            }
        case 0x10:                                                                          // 0x1nnn
            type = TokenType.Single("jmp", self.nnn())
        case 0x20:                                                                          // 0x2nnn
            type = TokenType.Single("jsr", self.nnn())
        case 0x30:                                                                          // 0x3xkk
            type = TokenType.Double("skeq", "V\(self.vX())", self.lo())
        case 0x40:                                                                          // 0x4xkk
            type = TokenType.Double("skne", "V\(self.vX())", self.lo())
        case 0x50:                                                                          // 0x5xy0
            type = TokenType.Double("skeq", "V\(self.vX())", "V\(self.vY())")
        case 0x60:                                                                          // 0x6xkk
            type = TokenType.Double("mov", "V\(self.vX())", self.lo())
        case 0x70:                                                                          // 0x7xkk
            type = TokenType.Double("add", "V\(self.vX())", self.lo())
        case 0x80:
            if self.lo() & 0x0F == 0x00 {                                                   // 0x8xy0
                type = TokenType.Double("mov", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x01 {                                            // 0x8xy1
                type = TokenType.Double("or", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x02 {                                            // 0x8xy2
                type = TokenType.Double("and", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x03 {                                            // 0x8xy3
                type = TokenType.Double("xor", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x04 {                                            // 0x8xy4
                type = TokenType.Double("add", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x05 {                                            // 0x8xy5
                type = TokenType.Double("sub", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x06 {                                            // 0x8xy6
                type = TokenType.Double("shr", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x07 {                                            // 0x8xy7
                type = TokenType.Double("rsb", "V\(self.vX())", "V\(self.vY())")
            } else if self.lo() & 0x0F == 0x0E {                                            // 0x8xyE
                type = TokenType.Double("shl", "V\(self.vX())", "V\(self.vY())")
            } else {
                type = nil
            }
        case 0x90:                                                                          // 0x9xy0
            type = TokenType.Double("skne", "V\(self.vX())", "V\(self.vY())")
        case 0xA0:                                                                          // 0xAnnn
            type = TokenType.Double("mvi", "I", self.nnn())
        case 0xB0:                                                                          // 0xBnnn
            type = TokenType.Double("jmi", "V0", self.nnn())
        case 0xC0:                                                                          // 0xCxkk
            type = TokenType.Double("rand", "V\(self.vX())", self.lo())
        case 0xD0:                                                                          // 0xDxyn
            type = TokenType.Triple("sprite", "V\(self.vX())", "V\(self.vY())", self.n())
        case 0xE0:
            if self.lo() == 0x9E {                                                          // 0xEx9E
                type = TokenType.Single("skpr", "V\(self.vX())")
            } else if self.lo() == 0xA1 {                                                   // 0xExA1
                type = TokenType.Single("skup", "V\(self.vX())")
            } else {
                type = nil
            }
        case 0xF0:
            if self.lo() == 0x07 {                                                          // 0xFx07
                type = TokenType.Double("gdelay", "V\(self.vX())", "DT")
            } else if self.lo() == 0x0A {                                                   // 0xFx0A
                type = TokenType.Double("key", "V\(self.vX())", "K")
            } else if self.lo() == 0x15 {                                                   // 0xFx15
                type = TokenType.Double("sdelay", "DT", "V\(self.vX())")
            } else if self.lo() == 0x18 {                                                   // 0xFx18
                type = TokenType.Double("ssound", "ST", "V\(self.vX())")
            } else if self.lo() == 0x1E {                                                   // 0xFx1E
                type = TokenType.Double("adi", "I", "V\(self.vX())")
            } else if self.lo() == 0x29 {                                                   // 0xFx29
                type = TokenType.Double("font", "F", "V\(self.vX())")
            } else if self.lo() == 0x33 {                                                   // 0xFx33
                type = TokenType.Double("bcd", "B", "V\(self.vX())")
            } else if self.lo() == 0x55 {                                                   // 0xFx55
                type = TokenType.Double("str", "[I]", "V\(self.vX())")
            } else if self.lo() == 0x65 {                                                   // 0xFx65
                type = TokenType.Double("str", "V\(self.vX())", "[I]")
            } else {
                type = nil
            }
        default:
            type = nil
        }

        return type
    }

    @discardableResult
    mutating internal func updateJump(_ label: String) -> Bool {
        guard self.containsJump.0 == true else {
            return false
        }

        switch self.type! {
        case .Single(let str, _):
            self.type = .Single(str, label)
            return true
        case .Double(let str, let d0, _):
            self.type = .Double(str, d0, label)
            return true
        default:
            return false
        }
    }

    // Return the lo byte of the opcode
    private func lo() -> UInt8  {
        return UInt8(self.opcode & 0x00FF)
    }

    // Return the hi byte of the opcode
    private func hi() -> UInt8 {
        return UInt8((self.opcode & 0xFF00) >> 8)
    }

    // Return the vX portion of an opcode
    private func vX() -> UInt8 {
        return self.hi() & 0x0F
    }

    // Return the vY portion of an opcode
    private func vY() -> UInt8 {
        return (self.lo() & 0xF0) >> 4
    }

    // Return the final nibble of an opcode
    private func n() -> UInt8 {
        return self.lo() & 0x0F
    }

    // Return the first three nibbles of an opcode
    private func nnn() -> UInt16 {
        return self.opcode & 0x0FFF
    }
}
