//
//  Parser.swift
//  Disassembler
//
//  Created by David Skuza on 11/15/16.
//  Copyright © 2016 David Skuza. All rights reserved.
//

import Foundation

// Possible errors when attempting to read data from disk
public enum ROMError: Error {
    case loadFailure            // The file does not exist
    case readFailure(String)    // There was an error reading the file
}

// ROM: Parses a file at a supplied path and returns its data, if it exists
public struct ROM {
    private var url: URL                                    // The URL of the file on disk
    private static let fileManager = FileManager.default    // Shared file manager, used to check for a valid file

    // Returns the filename of the supplied file (e.g ~/Skuza/ROMs/CHIP-8/PONG -> PONG)
    public var filename: String {
        get {
            return self.url.lastPathComponent
        }
    }

    // Returns the filepath of the supplied file
    public var path: String {
        get {
            return self.url.path
        }
    }

    // Initialize a ROM with a path to the ROM
    init(_ path: String) {
        self.url = URL(fileURLWithPath: path)
    }

    // Attempt to read a ROM from its supplied filepath
    // Throws an error if the file does not exist, or if there was an error reading the file
    public func read() throws -> Data {
        // Ensure that the file exists on disk
        guard ROM.fileManager.fileExists(atPath: self.path) else {
            throw ROMError.loadFailure
        }

        // Attempt to ready the file, throwing a ROMError with the localized description of the underlying error
        do {
            return try Data(contentsOf: self.url)
        } catch let error as NSError {
            throw ROMError.readFailure(error.localizedDescription)
        }
    }
}
